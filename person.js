function init_person(){
    var user_nickname='';
    var user_email = '';
  firebase.auth().onAuthStateChanged(function (user) {
    
    if (user) {
        upload_photo(user);
        get_img(user);
        user_email = user.email;
        user_nickname = user.displayName;

        console.log(user);   
    } else {
      
    }
});
}

function upload_photo(user){

    document.getElementById('upload_photo_btn').innerHTML = "<input id = 'uploadinput' type='file'></input><label class='btn btn-primary' for='uploadinput'>Your Photo</label>";
    var uploadFileInput = document.getElementById("uploadinput");
    console.log(user.email);
    var storageRef = firebase.storage().ref();
    console.log(user.email);
    uploadFileInput.addEventListener("change", function(){
        var file = this.files[0];
        var uploadTask = storageRef.child(user.email+".png").put(file);
        uploadTask.on('state_changed', function(snapshot){
           
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: 

                console.log('Upload is paused');
                break;
                case firebase.storage.TaskState.RUNNING: 

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            console.log("Error!");
        }, function() {
            var downloadURL = uploadTask.snapshot.downloadURL;
            // location.reload();
            get_img(user);
            user.updateProfile({
                photoURL: downloadURL
            })
        });
    },false);
}
function get_img(user){
    //var getPicBtn = document.getElementById("getPicBtn");
    //var downloadedImg = document.getElementById("downloadedImg");
    var storageRef = firebase.storage().ref();
    var image_url ='';
    //getPicBtn.addEventListener("click", function(){
    var temp = [];
    temp[temp.length] = '<div class="fa-3x"><i class="fas fa-spinner fa-spin"></i></div>';
    document.getElementById('downloadedImg').innerHTML = temp.join('');
    var pathReference = storageRef.child(user.email+'.png');
    pathReference.getDownloadURL().then(function(url) {
        console.log(url);
        image_url = url;
        document.getElementById("downloadedImg").innerHTML = "<img src='"+image_url+"' class='img-circle' width='304' height='236' style='display:block; margin:auto; position:center;'></div>";
    })
}








window.onload = function () {
    init_person();
};
