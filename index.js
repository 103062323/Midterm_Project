function ValidateNumber(e, pnumber)
{
    if (!/^\d+$/.test(pnumber))
    {
        var newValue =/^\d+/.exec(e.value);         
        if (newValue != null)         
        {             
            e.value = newValue;        
        }      
        else     
        {          
            e.value = "";    
        } 
    }
    return false;
}

window.onload=function(){   


  //使用Popup註冊FB方式
  var btnLoginFBPop = document.getElementById('btnLoginFBPop');
  var provider = new firebase.auth.FacebookAuthProvider();

  
  btnLoginFBPop.addEventListener('click', e => {
    firebase.auth().signInWithPopup(provider).then(function (result) {
        console.log('signInWithPopup');
        var token = result.credential.accessToken;
        var user = result.user;
        create_alert('success','You have logged in with FB successfully!')
        setTimeout('location.href="index.html"', 2000);
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
        console.log(errorMessage);
        create_alert('error','Unexpected error occurs.');
    });
  });

 //使用Popup註冊Google方式
 var btnGoogle = document.getElementById('btngoogle');
  var provider2 = new firebase.auth.GoogleAuthProvider();
  btnGoogle.addEventListener('click', function () {
      console.log('signInWithPopup');
      firebase.auth().signInWithPopup(provider2).then(function (result) {
          var token = result.credential.accessToken;
          var user = result.user;
          console.log('sucess');
          create_alert('success','You have logged in with Google successfully!')
          setTimeout('location.href="index.html"', 2000);
      }).catch(function (error) {
          console.log('error');
          create_alert('error','Unexpected error occurs.');
      });
  });

  function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
  }
  var list_area = document.getElementById('list');
  var symbol_area = document.getElementById('symbol');

  var user_nickname='';
  var user_email = '';
  firebase.auth().onAuthStateChanged(function (user) {
    var menu = document.getElementById('d_menu');
    var welcome = document.getElementById('user_welcome');
    if (user) {
        user_email = user.email;
        user_nickname = user.displayName;
        var user_photo = user.photoURL;
        console.log(user_photo);
        welcome.innerHTML = "<img src='"+ user_photo + "'style='width: 30px; height: 30px; border-radius: 50%;'> "   + user_nickname +   "  歡迎回來！" ;
        console.log(user);

        menu.innerHTML = 
        "<span class='dropdown-item'>" + user.email 
        + "</span><a href='menu.html'><span class='dropdown-item'>菜單瀏覽</span></a>"
        + "<a href='list.html'><span class='dropdown-item'>訂位歷程</span></a>"
        + "<a href='person.html'><span class='dropdown-item'>個人頁面</span></a>"
        + "<a href='contact.html' target='_blank'><span class='dropdown-item'>聯絡我們</span></a>"
        + "<span class='dropdown-item' id='logout-btn'>登出</span>";
        
        

        var signoutSmtBtn = document.getElementById("logout-btn");
        signoutSmtBtn.addEventListener("click",function(){
            firebase.auth().signOut().then(function() {
            create_alert('success','You have signed out successfully!')
            console.log("User sign out!");
            }, function(error) {
            console.log("User sign out error!");
            create_alert('error','Unexpected error occurs.')
            })
        },false);            

    } else {
        menu.innerHTML = 
        "<a class='dropdown-item' href='log.html'>註冊</a>"
        +"<a class='dropdown-item' href='login.html'>登入</a>"
        +"<a class='dropdown-item' onclick='notifyMe()'>訂閱</a>";

        document.getElementById('comment_area').innerHTML = "";

        // It shows up if not login
        //symbol_area.innerHTML = "<a href='#' id = 'btnLoginFBPop'><i class='fab fa-facebook text-white mr-1'></i></a> <a href='#' id='btngoogle'><i class='fab fa-google text-white mr-2'></i></a>";
       // It won't show any post if not login
       welcome.innerHTML = "首頁";
       list_area.innerHTML = '';
        //document.getElementById('post_list').innerHTML = "";
    }

});

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    post_btn.addEventListener('click', function () {
        var date = new Date();
        if (post_txt.value != "") {

            var data = {
                email : user_email,
                post_data : post_txt.value,
                current_date : date + "",
                name : user_nickname
            };
            firebase.database().ref("com_list").push(data);
            post_txt.value = "";
        }
    });
    
    var str_before_username = "<div class='my-3 p-3 bg-white rounded border border-info' style='display:block'><h4 id='recent' class='border-bottom border-gray pb-2 mb-0'>Customer Comment</h4>"
    
    + "<p class='media-body py-2 mb-0 small lh-125 border-bottom border-gray' style='display:block;font-style: oblique;font-size:14px'><strong class='d-block text-secondary'>";
    
    var str_after_content = "</pre></div>\n";
    
    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                total_post.unshift(str_before_username + 
                  childData.name +"&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" + childData.current_date + "</strong></p>"
                  +"<p class='pr-5 py-2' style=' font: bold 20px 微軟正黑體,Georgia, serif;   color: rgb(77, 124, 255); display:block;'>"+ childData.post_data + str_after_content);
                first_count += 1;
            })});
    
    
            document.getElementById('post_list').innerHTML = total_post.join('');
    
            postsRef.on('child_added',function(data){
                second_count += 1; 
                if(second_count > first_count){
                    var childData = data.val();
                    total_post.unshift(str_before_username + 
                      childData.name +"&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  "+ childData.current_date+ "</strong></p>" 
                      +"<pre class='pr-5 py-2' style='font:bold 20px 微軟正黑體,Georgia, serif;   color: rgb(77, 124, 255); display:block;'>"+ childData.post_data + str_after_content); 
                    document.getElementById("post_list").innerHTML = total_post.join('');
                }
            })

  //訂位確認
  var check_for_reservation = document.getElementById('check');

  var validation = document.getElementsByClassName
  check_for_reservation.addEventListener('click',function(){
    var input_name = document.getElementById('username').value;
    var input_tel = document.getElementById('tel').value;
    var input_count = document.getElementById('email').value;
    var input_date = document.getElementById('date').value;
    if(input_name != "" && input_tel != "" && input_count != "" && input_date != "")
    {
      console.log('check');
      var User = firebase.auth().currentUser;
      var profiles = {
        user: User.uid,
        email: User.email,
        name: input_name,
        tel: input_tel,
        count: input_count,
        date: input_date
      };  

      var Userupdate = {
        email:User.email,
        name:input_name,
        tel:input_tel,
        count:input_count,
        date:input_date
      }
      var User_Update = {};
      var newKey = firebase.database().ref().child('information').push().key;
      User_Update['user/'+User.uid+'/'+newKey] = Userupdate;

      var Update = {};
      
      Update['/information/'+newKey]=profiles;
      firebase.database().ref().update(User_Update).then(function(){
        document.getElementById('username').value = "";
        document.getElementById('tel').value = "";
        document.getElementById('email').value = "";
        document.getElementById('date').value = "";
        create_alert('success','Wish you have a nice day!');
        setTimeout('location.href="index.html"', 2000);
      }).catch(function (error) {
        console.log('error');
        document.getElementById('username').value = "";
        document.getElementById('tel').value = "";
        document.getElementById('email').value = "";
        document.getElementById('date').value = "";
        create_alert('error','Your format is invalid.');
      });
      
      firebase.database().ref().update(Update);
    }
    else{console.log('not');
    console.log(input_date);
    console.log(input_count);
    console.log(input_name);}
  });



      
    
  //Email驗證
  /*var verifyBtn = document.getElementById("verifyBtn");
  verifyBtn.addEventListener("click",function(){
    user.sendEmailVerification().then(function() {
      console.log("驗證信寄出");
    }, function(error) {
       console.error("驗證信錯誤");
    });
  },false);*/
  
  /*更改密碼
  var chgPwd = document.getElementById("chgPwd");
  var chgPwdBtn = document.getElementById("chgPwdBtn");
  chgPwdBtn.addEventListener("click",function(){
    firebase.auth().sendPasswordResetEmail(chgPwd.value).then(function() {
      // Email sent.
      console.log("更改密碼Email已發送");
      chgPwd.value = "";
    }, function(error) {
      // An error happened.
      console.error("更改密碼",error);
    });
  },false);
  
  //查看目前登入狀況
  var user;
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      user = user;
      console.log("User is logined", user)
    } else {
      user = null;
      console.log("User is not logined yet.");
    }
  });
  
  //如果使用者操作了更改密碼、刪除帳號、更改信箱等，需要再次驗證
  /*var user = firebase.auth().currentUser;
  var credential = firebase.auth().EmailAuthProvider.credential(
    user.email,
    //password from user
  )*/
  
  /*var provider = new firebase.auth.FacebookAuthProvider();
  /所需授權的Scope 
  //查閱 https://developers.facebook.com/docs/facebook-login/permissions
  provider.addScope('user_birthday');
  provider.setCustomParameters({
    'display': 'popup'
  });
  */
};

function notifyMe() {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('New Page!', {
      icon: 'img/notice.png',
      body: "Welocome to Corner! Wish you have a nice day.",
    });

    notification.onclick = function () {
      window.open("http://www.cornercaffe.com.tw/Corner/");      
    };

  }

}