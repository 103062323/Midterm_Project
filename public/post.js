function initial(){
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var data = {
                email : user_email,
                post_data : post_txt.value
            };
            firebase.database().ref("com_list").push(data);
            post_txt.value = "";
        }
    });
    
    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    
    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + 
                "</strong>" + childData.post_data + str_after_content;
                first_count += 1;
            })});
    
    
            document.getElementById('post_list').innerHTML = total_post.join('');
    
            postsRef.on('child_added',function(data){
                second_count += 1;
                if(second_count > first_count){
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + 
                    childData.email + "</strong>" + childData.post_data + str_after_content;
                    document.getElementById("post_list").innerHTML = total_post.join('');
                }
            })
}

window.onload = function()
{
    initial();
}