# Software Studio 2018 Spring Midterm Project


## Topic
* [小時候最愛的餐廳，臺南Corner餐廳訂位網頁]

* Key functions
    1. [ 即時留言comment區 ]
    2. [ 訂位表單填寫 ]
    3. [ 訂位紀錄歷程 ]
    4. [ 聯絡我們/Contact us 表單 ]

* Other functions
    1. [ 個人大頭貼置換(Cloud storage) ]
    2. [ 目前登入狀況確認(暱稱+大頭貼) ]
    3. [ 紀錄即時留言發文時間 ]
    5. [ 重要頁面未登入時隱藏 ]
    6. [ 表單輸入的validation ]
    7. [ 餐廳地圖嵌入/菜單瀏覽滑動頁面 ]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description  

  

# *Key functions*
    
### **1.頁底即時留言區**
---
起初的用意是讓使用者可以輸入一些feedback，然後記錄在realtime database中，後來決定將其功能移植到Contact us中  
所以此功能就改成像是發文(Post)一樣，可以在左半側輸入文字內容，並且在右側即時更新發文訊息：

![Alt text](img/1.png)

如上圖，資料讀取以監聽button的方式實行，只要按下就會將data push進"com_list"的分支中，並且清除輸入文字。  
而資料顯示的方式我用unshift()更換它的顯示順序，並且用on去偵測改變，若有子項被加入的話，就會置最新的文章在右側畫面最上方：

![Alt text](img/2.png)

如上圖，一旦有新的文章發佈，就會使用join()去更改發文區塊的innerHTML。    

而實際的功能畫面如下，右側的文章一開始過長，有用overflow:auto改成滾輪模式，使用者可以以滾輪上下移動。  
每篇文章在發佈時也都會使用Time()紀錄發佈時間，並且將當錢發文時間和發佈者暱稱放在方塊中。


![Alt text](img/3.png)
 
### **2.訂位表單填寫**
---

![Alt text](img/4.png)

左側使用了google map的內嵌功能，將絕對位置定位在餐廳上並且嵌入網頁，右側則是能輸入訂位的相關資訊。  


![Alt text](img/5.png)

在資料的存取上，由於每個會員都會紀錄訂位歷程在自己的專屬頁面，所以資料存放在各自的User.uid裡，這樣訂位歷程就不會被別人讀取。  
在資訊的驗證上也有些著墨，other functions中會提到~

### **3.訂位紀錄歷程**
---

![Alt text](img/7.png)

如上圖，登入後，在個人的歷程專頁中就能看到自己所填寫過所有的訂位資訊。

![Alt text](img/6.png)

這邊使用助教Lab6中的first_count和second_count的方式去更新，以免過多的資訊導致網頁讀寫過慢。  
同樣地，也用了unshift()的方式讓最新的訂位紀錄保持在網頁最上方，這些資料都放在個人的uid下，所以不同的會員會只會有各自的資料顯示。

### **4.Contact us**
---

![Alt text](img/8.png)

對於較長的建議或著是留言，在頁底即時的更新comment區顯示上會有點不美觀  
儘管對於過長字串的部分，有用css和tag pre妥善處理，可是終究還是會有有字量上限的問題   
所以就增加了這個功能，可以將所有的資訊和建議存入'feedback'底下，同時順便儲存使用者資料，以供參考。

# *Spec. Basic Component*

### **1.Membership Mechanism**
---

### ***註冊***
![Alt text](img/9.png)

註冊頁面共有四個資料，若已經有帳號可以切進Login的頁面。

![Alt text](img/10.png)

比較特別的是名字的更新並不是送到database裡面，而是使用updateProfile()更新至內建的displayName中。

### ***登入***

![Alt text](img/11.png)

登入頁面，如果有Google或是FB帳號，亦可以直接點選下方連結，將會在確認登入後自動跳轉回主頁。

### ***登出***

![Alt text](img/12.png)

在主頁的右上角有會員選單，登入後的下拉視窗如上，點選'登出'後即可sign out。  
p.s.選單中的所有功能只有在登入之後才會顯示，否則在未登入的狀況下，只會出現下圖三種：

![Alt text](img/13.png)

上述登入和註冊功能也是從此點選跳轉。

### **2.Database**
---
![Alt text](img/14.png)

上圖為後臺的database槽狀圖
com_list是頁底即時留言功能、feedback是contact us的表單內容、information是備份的訂位資料、user底下的專屬內容是訂位歷程。


### **3.RWD**
---
![Alt text](img/15.png)

排版的部分為了讓手機或者是小畫面也能順利顯示，使用了bootstrap的功能，  
基本上都是用class="container/container-fluid"，然後用row跟col-md/sm-x去切割畫面。

![Alt text](img/16.png)

但是像這種在小畫面中的toggle就比較麻煩了，在mobile上很容易錯位或是點不到，這時候jQuery就派上用場了。   
先以選單按鈕[三條橫線]為監聽對象$('#nav-toggle').on，當被按下的時候讓$('#main-nav').toggleClass("open");打開就行了。

# *Spec. Advanced Component*

### **1.Third-Party Sign In/Chrome Notification**
---
![Alt text](img/17.png)

在基本功能中已經有提到可以從登入頁面中使用FB或Google登入，  
此外，在頁底的部分，有放置font-awesome的圖示按鈕，點選之後會自動登入且reload整個頁面(為了顯示表單和留言區)。   

而鈴鐺鈕便是Chrome Notification的部分，在進入主頁時就會自動onload是否允許/封鎖通知  
若是在'granted'下按下選單中的訂閱或是此圖示，將會在螢幕右下跳出通知，點選可以進入轉角官方網站的頁面。

### **2. CSS Animation**
---
![Alt text](img/18.png)

在上傳大頭貼的時候，若程序正在執行中，將會使用轉圈圈的動畫圖示代表等待。

![Alt text](img/19.png)

在一進入主頁的時候，文字方塊會以影格(@keyframes)的方式從右側縮放滑入。


# *Other functions*

### **1.個人大頭貼(Cloud storage) + 目前登入狀況確認(暱稱+大頭貼)**
---

![Alt text](img/20.png)

使用了firebase雲端儲存的方式，可以讓使用者自訂上傳喜歡的圖片做為大頭貼，  
上傳圖片的方式和小畫家相同，用label指向input，然後再讓input(button type)的display:hidden即可。

![Alt text](img/21.png)

除了將照片存入雲端之外，我亦將使用者上傳的照片用updateProfile()的方式存入內置的photoURL，  
這樣需要使用的時候，就可以以Current.user.photoURL的形式去取得圖片的路徑。

![Alt text](img/22.png)

於是，在主頁的bar中，就可以運用剛才已經存入的照片+nickname去確認現在登入的狀況，或者是帳號是否正確了。


### **2.菜單滑動頁面**
---

![Alt text](img/23.png)

![Alt text](img/24.png)

在主頁介紹以及Menu中使用了bootstrap中的data-slide-to='x'製作可滑動頁面，讓整體版面比較活潑一點。  
BTW，每一張照片還有內文都是親自找和用小時候去吃大餐時的印象刻出來的QQ，比想像中還耗費時間XD，  
此外在主頁上也有外連Corner steak house官方的網頁，雖然感覺上已經很久沒有更新了。


### **3.重要頁面未登入時隱藏**
---

![Alt text](img/25.png)

![Alt text](img/26.png)

為了安全性還有避免原始碼外顯，若處於登出的狀態，頁面中的留言和訂位表單都不會出現，  
主頁中只會出現自製的訂位步驟，以及可以外連到'菜單瀏覽'頁面的圖示。  
  
同樣地，訂位歷程以及個人頭貼頁面也將會隱藏顯示，直至有使用者重新登入。


### **4.註冊/表單輸入時的validation**
---

![Alt text](img/27.png)

在訂位表單的設計中，使用了checkValidity()以回報錯誤資訊，若有日期上的謬誤(e.g.11/31)或者有項目未填，  
將會直接event.preventDefault()和stopPropagation()，然後將預先寫好的錯誤資訊在每一行input下方顯示。  

![Alt text](img/28.png)

同時，為了避免在電話或者是訂位人數等數字類表格中輸入非法文字，實作了上圖的function然後把它放進onkeyup=''中，  
若是輸入非數字類的字元，將會立即被刪除，以達成正確輸入的目的。

---


## Security Report (Optional)

![Alt text](img/29.png)

附上後臺規則以利說明～  

在大架構中，我本來限制了只有登入的人才可以進行讀寫，但因為讀寫規則是由潛層覆蓋深層，  
所以深層內的規則很容易搞混，有些容易等於default，為了日後修改方便，所以就加入細項。  
   
在"user"的欄位中，會以uid去進行分類，在此分類下代表訂位的個資，所以在讀寫上限制的比較嚴格。  

而在一個字的前方加上'$'可以自定義，所以"$listid"意即針對user內的$uid中的所有key去做規則限制  
而我每一個"$listid"下都具有五個訂位時需要填入的資料，有了類似指標的東西之後  
就可以用".validate"去限制每一個新增資料(newData.child('xx'))該有的屬性－－  
例如tel代表電話號碼，它的長度必須剛好等於10；count是訂位人數，它的個數不得超過9個人；  
最後用isString()去判斷email和name的屬性是否為字串，  
**假設上述有其一不成立，那麼這筆訂位資料就不會寫進database中。**

同理，在"com_list"的槽狀中，裝了頁底留言系統的資料，由於此資料有點像是公開發文，所以我並沒有對讀取加以限制。
和"user"一樣，運用".validate"限制每篇發文的文字數量不得超過120個字，這大概是剛好會超出範圍(啟用滾輪)的字數。

最後是「聯絡我們」功能，在"feedback"中的讀寫因為隱私的關係，所以讀取上用false，而這個槽狀中的'user'是指uid，!=null是為了保證會員是登入狀態的，而非用不正常手段進入contact介面。