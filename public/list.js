function List_Init(){
    firebase.auth().onAuthStateChanged(function(user){
        console.log(user);
        var html_head = "<div><p class = 'title_title'>";
        var html_mid = "</p><p class='title_txt'>";
        var html_end = "</p></div>";
        var userRef = firebase.database().ref('user/'+user.uid);
        var count_origin = 0;
        var count_new = 0;
        userRef.once('value').then(function(snapshot){
            var list = document.getElementById('list_area');
            snapshot.forEach(function(childsnapshot){
                count_origin ++;
                var child_data = childsnapshot.val();
                var list_data = document.createElement('div');
                list_data.setAttribute('class','panel panel-danger');
                list_data.innerHTML = "<div class= 'panel-heading' id='big_title'>Recent information</div><div class = 'panel-body'>"+html_head + "Your Reservation Name:" + html_mid + child_data.name + html_end 
                    + html_head + "Your email address:" + html_mid + child_data.email + html_end 
                    + html_head + "Your cellphone number:" + html_mid + child_data.tel + html_end 
                    + html_head + "The numbers of member:" + html_mid + child_data.count + html_end 
                    + html_head + "Reservation date:" + html_mid + child_data.date + html_end +"</div>";
                list.insertBefore(list_data,list.childNodes[0]);
            });
            userRef.on('child_added',function(snapshot){
                var child_data = snapshot.val();
                count_new++;
                if(count_new>count_origin){
                    var list_data = document.createElement('div');
                    list_data.innerHTML = html_head + "Name:" + html_mid + child_data.name + html_end 
                        + html_head + "email:" + html_mid + child_data.email + html_end 
                        + html_head + "telephone:" + html_mid + child_data.tel + html_end 
                        + html_head + "member:" + html_mid + child_data.count + html_end 
                        + html_head + "date:" + html_mid + child_data.date + html_end ;
                    list.insertBefore(list_data,list.childNodes[0]);
                }
            });
        })
        
    })
}
window.onload = function(){
    List_Init();
}