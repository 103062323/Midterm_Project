function initLogIn()
{

    //登入
    var accountL = document.getElementById("accountL");
    var pwdL = document.getElementById("pwdL");
    var loginSmtBtn = document.getElementById("loginSmtBtn");
    loginSmtBtn.addEventListener("click",function(){
    console.log(accountL.value);
    firebase.auth().signInWithEmailAndPassword(accountL.value, pwdL.value).then(function(user){
            alert("You have signed in successfully.");
            location.href="index.html";
            //user can be used
        }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage);
        alert(errorMessage);
    })
    },false);

    
    //使用Popup註冊FB方式
    var btnLoginFBPop = document.getElementById('btnLoginFBPop');
    var provider = new firebase.auth.FacebookAuthProvider();

    
    btnLoginFBPop.addEventListener('click', e => {
        firebase.auth().signInWithPopup(provider).then(function (result) {
            console.log('signInWithPopup');
            var token = result.credential.accessToken;
            var user = result.user;
            alert('You have logged in with FB successfully!');
            setTimeout('location.href="index.html"', 2000);
            
            
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            console.log(errorMessage);
            alert('Unexpected error occurs.')
            
        });
    });

    //使用Popup註冊Google方式
    var btnGoogle = document.getElementById('btngoogle');
    var provider2 = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function () {
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider2).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log('sucess');
            alert('You have logged in with Google successfully!');
            setTimeout('location.href="index.html"', 2000);
        }).catch(function (error) {
            console.log('error');
            alert('Unexpected error occurs.');
        });
    });

}
window.onload = function () 
{
    initLogIn();
}
